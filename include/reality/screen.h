#ifndef REALITY_SCREEN_H
#define REALITY_SCREEN_H

#include <stdint.h>
#include <stddef.h>
#include <string.h>
#include <util.h>


static const size_t VGA_WIDTH = 80;
static const size_t VGA_HEIGHT = 25;

size_t termRow;
size_t termCol;
uint8_t termColor;
uint16_t* termBuffer;

static inline uint16_t vgaEntry (unsigned char uc, uint8_t color)
{
	return (uint16_t) uc | (uint16_t) color << 8;
}


void moveCursor(uint16_t cPos)
{
	outb(0x3d4, 0x0f);
	outb(0x3d5, (uint8_t) cPos);
	outb(0x3d4, 0x0e);
	outb(0x3d5, (uint8_t)(cPos >> 8));
}

void termInit(void)
{
  size_t index;

  termRow = 0;
  termCol = 0;
  termColor = 0x07;
  termBuffer = (uint16_t*) 0xB8000;


  for(size_t y = 0; y < VGA_HEIGHT; y++) {
    for(size_t x = 0; x<VGA_WIDTH; x++) {
        index = y * VGA_WIDTH + x;
        termBuffer[index] = vgaEntry(' ', termColor);
    }
  }
}

void clear(void) {termInit();}

void putchar(char uc)
{

  size_t index;

  switch(uc) {
    case '\n':
      termRow++;
      termCol = 0;
      break;
		case '\t':
			termCol += 5;
			break;
    default:
      index = termRow * VGA_WIDTH + termCol;
      termBuffer[index] = vgaEntry(uc, termColor);
      termCol++;
  }

	index = termRow * VGA_WIDTH + termCol;
	moveCursor(index);
}

void print(char* str)
{
  for(size_t i=0; i<strlen(str); i++) {
    putchar(str[i]);
  }
}

#endif
