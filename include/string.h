#ifndef STRING_HEADER
#define STRING_HEADER

#include <stddef.h>

size_t strlen(char* str)
{
  size_t length;
  for(length = 0; str[length] != '\0'; length++);
  return length;
}

#endif
